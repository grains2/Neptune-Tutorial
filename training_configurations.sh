#!/bin/bash

# configurations with Adam optimizer
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.01 --optimizer Adam --epochs 10 --exp_name auto --base_path . --output_path output/conf_1
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.001 --optimizer Adam --epochs 10 --exp_name auto --base_path . --output_path output/conf_2
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.0001 --optimizer Adam --epochs 10 --exp_name auto --base_path . --output_path output/conf_3
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.00001 --optimizer Adam --epochs 10 --exp_name auto --base_path . --output_path output/conf_4

# configurations with SGD optimizer
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.01 --optimizer SGD --epochs 10 --exp_name auto --base_path . --output_path output/conf_5
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.001 --optimizer SGD --epochs 10 --exp_name auto --base_path . --output_path output/conf_6
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.0001 --optimizer SGD --epochs 10 --exp_name auto --base_path . --output_path output/conf_7
python train.py --nept_token $NEPTUNE_API_TOKEN --learning_rate 0.00001 --optimizer SGD --epochs 10 --exp_name auto --base_path . --output_path output/conf_8