import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint

import numpy as np
import matplotlib.pyplot as plt
import argparse
import neptune
from pathlib import Path
import os
import pickle
import imageio

# convinient utility to log multliple metrics
def log_history_metric(name, values):
        for i in range(len(values)):
            neptune.log_metric(name, values[i])

# command line arguments
parser = argparse.ArgumentParser()

parser.add_argument("--nept_token", type=str, dest="nept_token", help='Neptune access token require to log experiments for a target account.', required=True)

parser.add_argument("--learning_rate", type=float,
                    dest="learning_rate", help="Learning rate, default 1e-3", default=1e-03)

parser.add_argument('--optimizer', choices=['Adam', 'SGD'], default='Adam',  help='Optimizer to use for back-propagation (Default Adam)',
                    dest='optimizer')

parser.add_argument("--epochs", type=int, dest="epochs",
                    help="Number of epochs, default 10.", default=10)

parser.add_argument('--exp_name', required=True, help="Experiment name, brief description used in Neptune. Write 'auto' for standard assigned name.",
                    dest='exp_name')

parser.add_argument('--base_path', help=' Experiment base path that contains all input files and output folders',
                    required=True, dest='base_path')

parser.add_argument("--output_path", dest="output_path",
                    help="Relative path to the output path, starting from base path. Inside the folder a directory with the timestamp will be created and filled",
                    required=True)

args = parser.parse_args()

# reformat arguments
if args.base_path[-1] != '/':
    args.base_path += '/'

if args.output_path[-1] != '/':
    args.output_path += '/'

# store arguments
learning_rate = args.learning_rate
optimizer = args.optimizer
epochs = args.epochs
exp_name = args.exp_name
nept_token = args.nept_token

# path building for outout files and directory
base_path = args.base_path
output_path = args.output_path
log_path = base_path + output_path + 'logs/'
log_net = log_path + 'net/'
log_weights = log_net + 'weights/'
os.makedirs(log_weights)

print("Creating Neptune experiment")

# connect to a specific neptune project for which you have access
neptune.init('GRAINS/Tutorial', api_token=nept_token)

# strings for parameters in the parameter tab
params = {'learning_rate': learning_rate,
          'epochs': epochs,
          'optimizer': optimizer,
          'loss': 'Sparse Categorical Crossentropy'}

# set the experiment name
if exp_name == 'auto':
    exp_name = 'Fashion-MNIST_' + str(learning_rate) + '_' + str(epochs) + '_' + optimizer

# create experiment with defined parameters, uploaded source code and tags
neptune.create_experiment(name=exp_name,
                          params=params,
                          upload_source_files=[str(Path(__file__).absolute())])

# add convenient tags for future filtering
neptune.append_tags('training', 'Fashion-MNIST', params['loss'], params['optimizer'])

# set the optimizer
if optimizer == 'Adam':
    optimizer =  Adam(learning_rate=learning_rate)
elif optimizer == 'SGD':
    optimizer = SGD(learning_rate=learning_rate)

# prepare the Fashio-MNIST dataset
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images / 255.0
test_images = test_images / 255.0

# build a simple sequential model
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10)
])

# callbacks
weights_path = log_weights + "weights.{epoch:02d}.hdf5"

tensorboard_callback = TensorBoard(log_dir=log_net, update_freq=1000, write_graph=False)

checkpoint = ModelCheckpoint(weights_path, monitor='val_loss', verbose=0,
                             save_best_only=False, save_weights_only=False, mode='auto', period=1)

callbacks = [checkpoint, tensorboard_callback]

# compile the model
model.compile(optimizer=optimizer,
              loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# training
history = model.fit(train_images, train_labels, epochs=epochs, validation_data=(test_images, test_labels), callbacks=callbacks)

# save the weigths
model.save(log_weights + 'model.h5')

# save the history
history_path = log_net + 'history.p'
with open(history_path, 'wb') as file_pi:
    pickle.dump(history.history, file_pi)

# log metrics on neptune
for h in history.history.keys():
    log_history_metric(h, history.history[h])

# upload history on neptune
neptune.log_artifact(history_path)

# plot a sample figure for dataset classes
plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])

# save the figure
plot_path = log_net + 'plot.png'
plt.savefig(plot_path)

# upload the figure on neptune
image = imageio.imread(plot_path)
neptune.log_image('Fashion-MNIST categories', image)