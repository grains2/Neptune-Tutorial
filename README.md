# Neptune Tutorial

The Neptune Tutorial is a cookiecutter project intended as an example to start using [neptune.ai](https://neptune.ai/) and log experiments efficiently.

## Installation

Use the environment manager [conda](https://docs.conda.io/en/latest/) to install the dependencies.

```bash
conda env create -f environment.yml
```

## Usage
Several configurations have been prepared in the file training_configurations.sh.

```bash
conda activate neptune_tutorial
./training_configurations.sh
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)